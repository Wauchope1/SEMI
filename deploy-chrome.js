const fs = require('fs');

const { zipName, archive } = require('./build.js');

const REFRESH_TOKEN = process.env.REFRESH_TOKEN;
const EXTENSION_ID = process.env.EXTENSION_ID;
const CLIENT_SECRET = process.env.CLIENT_SECRET;
const CLIENT_ID = process.env.CLIENT_ID;

const webStore = require('chrome-webstore-upload')({
  extensionId: EXTENSION_ID,
  clientId: CLIENT_ID,
  clientSecret: CLIENT_SECRET,
  refreshToken: REFRESH_TOKEN,
});

(async () => {
  try {
    await archive.finalize();
    await webStore.uploadExisting(fs.createReadStream(zipName));
    await webStore.publish().then(console.log);
  } catch (err) {
    console.error(err);
    process.exit(1);
  }
})();

(() => {
    const id = 'auto-smith-bars';
    const title = 'AutoSmith Bars';
    const desc = 'AutoSmith Bars will cycle through your smithing bars and smelt those you have materials for.';
    const imgSrc = 'assets/media/bank/dragonite_bar.png';
    const skill = 'Smithing';

    const config = {
        ignoreIron: false,
    };

    // Converts the bar recipe map into an iterable list
    const bars = [...smithingBarRecipes.values()];

    // Start with Bronze
    let currentBar = 0;

    const moveToNext = () => {
        // Are we going to shift out of bounds?
        if (currentBar + 1 >= bars.length) {
            currentBar = 0;
        } else {
            currentBar += 1;
        }

        // Select the next bar in the recipe list
        game.smithing.selectRecipeOnClick(currentBar);
    };

    const autoSmithBars = () => {
        // Forces the system to open to the tab I want
        smithingSelectionTabs.forEach((tab) => {
            tab.hide();
        });

        // Pull up the bars tab
        smithingSelectionTabs[0].show();

        const ignoringIron = SEMI.getValue(id, 'ignoreIron') && items[bars[currentBar].itemID].name === 'Iron Bar';

        // Selects Bronze Bar by default
        game.smithing.selectRecipeOnClick(0);

        // Iterates through bars, making sure we can smith them and ensuring that if we can't we move to the next one
        for (const _ in bars) {
            if (game.smithing.level < bars[currentBar].level || ignoringIron) {
                moveToNext();
            }

            let itemCosts = bars[currentBar].itemCosts;
            for (const item in itemCosts) {
                if (getBankQty(item.id) < item.qty) {
                    moveToNext();
                    break;
                }
            }

        }

        if (!SEMIUtils.isCurrentSkill(skill)) {
            game.smithing.start();
        }
    };

    const onDisable = () => {
        SEMIUtils.stopSkill(skill);
    };

    const hasConfig = true;
    const configMenu = `<div class="form-group">
        <div class="custom-control custom-switch mb-1">
            <input type="checkbox" class="custom-control-input" id="${id}-iron-toggle" name="${id}-iron-toggle">
            <label class="custom-control-label" for="${id}-iron-toggle">
                Ignore Iron (for Steel smelting)
            </label>
        </div>
    </div>`;
    const saveConfig = () => {
        const toggled = $(`#${id}-iron-toggle`).prop('checked');
        SEMI.setValue(id, 'ignoreIron', toggled);
        SEMI.setItem(`${id}-config`, SEMI.getValues(id));
        SEMIUtils.customNotify(imgSrc, 'AutoSmith Bars config saved!');
    };
    const updateConfig = () => {
        $(`#${id}-iron-toggle`).prop('checked', SEMI.getValue(id, 'ignoreIron'));
    };

    SEMI.add(id, {
        onLoop: autoSmithBars,
        onDisable,
        config,
        hasConfig,
        configMenu,
        saveConfig,
        updateConfig,
        title,
        desc,
        imgSrc,
        skill,
    });
})();

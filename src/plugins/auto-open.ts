(() => {
    const pluginKind = 'open';

    const id = `auto-${pluginKind}`;
    const title = 'AutoOpen';
    const desc = 'AutoOpen is a script for opening containers like chests.';
    const imgSrc = 'assets/media/bank/elite_chest.png';

    let isItemEnabledToOpen = {};

    const fadeAll = () => {
        for (let i = 0; i < SEMIUtils.itemsLength(); i++) {
            const x = $(`#${id}-img-${i}`);
            if (x.length === 0) {
                continue;
            }
            const shouldBeFaded = !isItemEnabledToOpen[i];
            const currentState = typeof x.css('opacity') === 'undefined' ? '1' : x.css('opacity');
            const isFaded = currentState === '0.25';
            const isCorrect = isFaded === shouldBeFaded;
            const neededOpacity = shouldBeFaded ? 0.25 : 1;
            if (!isCorrect) {
                x.fadeTo(500, neededOpacity);
            }
        }
    };

    /** @param {number} i */
    const toggleAutoEnabled = (i) => {
        isItemEnabledToOpen[i] = !isItemEnabledToOpen[i];
        fadeAll();
    };

    const emptyObj = { media: 'assets/media/main/question.svg', name: '???' };
    const el = (i) => {
        const empty = !SEMIUtils.isItemFound(i);
        const { media, name } = empty ? emptyObj : items[i];
        const e = $(
            `<img id="${id}-img-${i}" class="skill-icon-md js-tooltip-enable" src="${media}" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="${name}">`
        );
        if (!empty) {
            e.on('click', () => toggleAutoEnabled(i));
        }
        return e;
    };

    const autoOpen = () => {
        const allButOneIsEnabled = SEMI.getItem(`${id}-sell-one`);

        if (SEMIUtils.isBankFull()) {
            return;
        }
        for (let i = bank.length - 1; i >= 0; i--) {
            const itemID = bank[i].id;
            if (isItemEnabledToOpen[itemID]) {
                let qty;
                if (allButOneIsEnabled) {
                    qty = bank[i].qty - 1;
                } else {
                    qty = bank[i].qty;
                }

                // If All But One reduces the count of available openings to 0, skip to next
                if (!qty) {
                    continue;
                }
                SEMIUtils.openItemWithoutConfirmation(itemID, qty);
                SEMIUtils.customNotify(
                    items[itemID].media,
                    `Opening ${numberWithCommas(qty)} of ${items[itemID].name}`,
                    { lowPriority: true }
                );
            }
        }
    };

    const setupContainer = () => {
        $(`#${id}-container`).html('');
        for (let i = 0; i < SEMIUtils.itemsLength(); i++) {
            if (!('dropQty' in items[i])) {
                continue;
            }
            $(`#${id}-container`).append(el(i));
        }

        const loadedAutoEnabled = SEMI.getItem(`${id}-config`);
        if (loadedAutoEnabled !== null) {
            if (Array.isArray(loadedAutoEnabled)) {
                for (let i = 0; i < loadedAutoEnabled.length; i++) {
                    if (loadedAutoEnabled[i]) {
                        isItemEnabledToOpen[i] = true;
                    }
                }
            } else {
                isItemEnabledToOpen = loadedAutoEnabled;
            }
        }

        setTimeout(() => fadeAll(), 500);
    };

    const onDisable = () => {
        $(`#${id}-status`).addClass('btn-danger');
        $(`#${id}-status`).removeClass('btn-success');
    };

    const onEnable = () => {
        $(`#${id}-status`).addClass('btn-success');
        $(`#${id}-status`).removeClass('btn-danger');
    };

    const autoShow = () => {
        $(`#modal-${id}`).modal('show');
    };

    const allButOneToggle = () => {
        const allButOneIsEnabled = SEMI.getItem(`${id}-open-one`);

        if (allButOneIsEnabled) {
            debugLog('All But One Disabled!');
            $(`#${id}-open-one`).addClass('btn-danger');
            $(`#${id}-open-one`).removeClass('btn-success');
        } else {
            debugLog('All But One Enabled!');
            $(`#${id}-open-one`).addClass('btn-success');
            $(`#${id}-open-one`).removeClass('btn-danger');
        }

        SEMI.setItem(`${id}-open-one`, !allButOneIsEnabled);
    };

    const debugLog = (msg) => {
        console.log(`[${title}] ${msg}`);
    };

    const injectGUI = () => {
        const x = $('#modal-item-log').clone().first();
        x.attr('id', `modal-${id}`);
        $('#modal-item-log').parent().append(x);
        const y = $(`#modal-${id}`).children().children().children().children('.font-size-sm');
        y.children().children().attr('id', `${id}-container`);

        const allButOne = SEMI.getItem(`${id}-open-one`);
        const controlSection = $(
            `<div class="col-12">
                <div class="row">
                    <div class="col-2">
                        <button class="btn btn-md btn-danger SEMI-modal-btn" id="${id}-status">Disabled</button>
                    </div>
                    <div class="col-2">
                        <button class="btn btn-md btn-${
                            allButOne ? 'success' : 'danger'
                        } SEMI-modal-btn" id="${id}-open-one">All But One</button>
                    </div>
                </div>
            </div>`
        );

        y.before(controlSection);

        const enableAutoButton = $(`button#${id}-status`);
        const enableAllButOneButton = $(`button#${id}-open-one`);

        enableAutoButton.on('click', () => SEMI.toggle(`${id}`));
        enableAllButOneButton.on('click', () => allButOneToggle());

        $(`#modal-${id} .block-title`).text(`${title} Menu`);

        $(`#modal-${id}`).on('hidden.bs.modal', () => {
            SEMI.setItem(`${id}-config`, isItemEnabledToOpen);
        });

        const btn = SEMI.components.RefreshButton(
            'Use this to refresh your openable items from your current item log.',
            setupContainer
        );
        x.find('.block-options').prepend(btn);

        setupContainer();
        setTimeout(() => {
            $(`#${id}-menu-button`).on('click', autoShow);
            $(`#${id}-menu-button`).attr('href', null);
        }, 1000);
    };

    SEMI.add(id, { ms: 2000, onLoop: autoOpen, onEnable, onDisable, title, desc });
    SEMI.add(id + '-menu', { title, desc, imgSrc, injectGUI });
})();

(() => {
    const id = 'auto-lute';
    const title = 'AutoLute';
    const desc = `AutoLute monitors your combat opponent's health and switches to Lute for the final kill of a dungeon, or the killing blow for normal combat, for the 5x GP reward.`;
    const imgSrc = 'assets/media/bank/almighty_lute.png';

    const autoLute = () => {
        const enemyHp = player.manager.enemy.hitpoints;
        const playerMaxHit = player.stats.maxHit;
        const isInDungeon = player.manager.areaData.type === 'Dungeon';
        const dungeon = isInDungeon ? DUNGEONS[player.manager.areaData.id] : undefined;
        const monster = player.manager.enemy.data.id;

        const isFinalMonster =
            dungeon !== undefined ? monster === dungeon.monsters[dungeon.monsters.length - 1] : false;
        const shouldSwitchToLute = dungeon !== undefined ? isFinalMonster : true;
        const lute = CONSTANTS.item.Almighty_Lute;
        const slot = 'Weapon';
        const wieldingLute = SEMIUtils.currentEquipmentInSlot(slot) === lute;
        const ableToKill = enemyHp > 0 && playerMaxHit >= enemyHp;
        const inCombat = player.manager.isInCombat;
        const newEnemyLoading = player.manager.fightInProgress;

        if (
            ableToKill &&
            shouldSwitchToLute &&
            !wieldingLute &&
            checkBankForItem(lute) &&
            newEnemyLoading &&
            inCombat
        ) {
            SEMIUtils.equipSwap(lute, slot);
        } else if ((enemyHp === 0 || !newEnemyLoading || !inCombat) && SEMIUtils.equipSwapConfig[slot].swapped) {
            SEMIUtils.equipSwap(0, slot);
        }
    };

    SEMI.add(id, {
        ms: 100,
        onLoop: autoLute,
        pluginType: SEMI.PLUGIN_TYPE.AUTO_COMBAT,
        title,
        imgSrc,
        desc,
    });
})();
